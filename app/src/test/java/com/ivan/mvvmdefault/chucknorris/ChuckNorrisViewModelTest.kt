package com.ivan.mvvmdefault.chucknorris

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.ivan.coopcrossword.utils.schedulers.BaseSchedulerProvider
import com.ivan.coopcrossword.utils.schedulers.ImmediateSchedulerProvider
import com.ivan.mvvmdefault.chucknorris.data.ChuckNorrisRepository
import com.ivan.mvvmdefault.chucknorris.model.internal.ChuckNorrisModel
import com.ivan.mvvmdefault.chucknorris.ui.ChuckNorrisState
import com.ivan.mvvmdefault.chucknorris.ui.DefaultState
import com.ivan.mvvmdefault.chucknorris.ui.ErrorState
import com.ivan.mvvmdefault.chucknorris.ui.LoadingState
import com.nhaarman.mockitokotlin2.verify
import io.reactivex.Single
import junit.framework.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.ArgumentCaptor
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.mockito.stubbing.OngoingStubbing

class ChuckNorrisViewModelTest {
    @Rule
    @JvmField
    var instantExecutorRule = InstantTaskExecutorRule()

    private val repository: ChuckNorrisRepository = Mockito.mock(ChuckNorrisRepository::class.java)

    val observerState = mock<Observer<ChuckNorrisState>>()

    private var schedulerProvider: BaseSchedulerProvider = ImmediateSchedulerProvider()
    private lateinit var viewModel: ChuckNorrisViewModel


    @Before
    fun setupMocksAndView() {
        MockitoAnnotations.initMocks(this)
        viewModel = ChuckNorrisViewModelFactory(
            repository,
            schedulerProvider
        ).create(ChuckNorrisViewModel::class.java)
    }

    @Test
    fun `requestData() loaded items`() {
        whenever(repository.provideChuckNorrisData()).thenReturn(Single.just(getDummyCrosswordInfo()))
        viewModel.uiState.observeForever(observerState)
        viewModel.requestChuckNorrisData()
        verify(repository).provideChuckNorrisData()

        val argumentCaptor = ArgumentCaptor.forClass(ChuckNorrisState::class.java)
        val expectedLoadingState = LoadingState(false)
        val expectedDefaultState = DefaultState(true, getDummyCrosswordInfo())

        argumentCaptor.run {
            verify(observerState, Mockito.times(2)).onChanged(capture())
            val (loadingState, defaultState) = allValues
            assertEquals(loadingState, expectedLoadingState)
            assertEquals(defaultState, expectedDefaultState)
        }
    }

    @Test
    fun `requestGameData() error loading items`() {
        val errorMessage = "error"
        whenever(repository.provideChuckNorrisData()).thenReturn(Single.error(Throwable(errorMessage)))
        viewModel.uiState.observeForever(observerState)
        viewModel.requestChuckNorrisData()
        verify(repository).provideChuckNorrisData()

        val argumentCaptor = ArgumentCaptor.forClass(ChuckNorrisState::class.java)
        val expectedLoadingState = LoadingState(false)
        val expectedErrorState = ErrorState(errorMessage, false)

        argumentCaptor.run {
            verify(observerState, Mockito.times(2)).onChanged(capture())
            val (loadingState, errorState) = allValues
            assertEquals(loadingState, expectedLoadingState)
            assertEquals(errorState, expectedErrorState)
        }
    }

    private fun getDummyCrosswordInfo(): ChuckNorrisModel {
        return ChuckNorrisModel(
            "1",
            "asd.com",
            "chuck norris value"
        )
    }

    private inline fun <T> whenever(methodCall: T): OngoingStubbing<T> = Mockito.`when`(methodCall)

    inline fun <reified T> mock(): T = Mockito.mock(T::class.java)
}
package com.ivan.mvvmdefault.chucknorris.ui

import SchedulerProvider
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.ivan.mvvmdefault.R
import com.ivan.mvvmdefault.api.ApiClient
import com.ivan.mvvmdefault.chucknorris.ChuckNorrisViewModel
import com.ivan.mvvmdefault.chucknorris.ChuckNorrisViewModelFactory
import com.ivan.mvvmdefault.chucknorris.data.ChuckNorrisRepository
import com.ivan.mvvmdefault.chucknorris.data.remote.ChuckNorrisRemoteDataSource
import com.ivan.mvvmdefault.databinding.FragmentChuckNorrisBinding
import kotlinx.android.synthetic.main.fragment_chuck_norris.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class ChuckNorrisFragment : Fragment() {
    private lateinit var binding: FragmentChuckNorrisBinding
    private val viewModel by viewModel<ChuckNorrisViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_chuck_norris, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        startLiveDataObservation()
        chuckNorrisNewJokeButton.setOnClickListener {
            viewModel.requestNewJoke()
        }
    }

    private fun startLiveDataObservation() {
        viewModel.requestChuckNorrisData()
        viewModel.uiState.observe(viewLifecycleOwner, Observer {
            it?.let { state ->
                when (state) {
                    is DefaultState -> {
                        binding.chuckNorris = state.chuckNorris
                    }
                    is LoadingState -> {
                        Toast.makeText(requireContext(), "Loading..", Toast.LENGTH_SHORT).show()
                    }
                    is ErrorState -> {
                        Toast.makeText(
                            requireContext(),
                            "Error ! ${state.errorMessage}",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }
            }
        })
    }

    companion object {
        const val TAG = "ChuckNorrisFragment"
        fun newInstance() = ChuckNorrisFragment()
    }
}

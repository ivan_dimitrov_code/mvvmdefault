package com.ivan.mvvmdefault.chucknorris.data

import com.ivan.mvvmdefault.chucknorris.model.internal.ChuckNorrisModel
import io.reactivex.Single

open class ChuckNorrisRepository(private val remoteDataSource: ChuckNorrisDataSource) {

    var cachedData: ChuckNorrisModel? = null

    open fun provideChuckNorrisData(): Single<ChuckNorrisModel> {
        return if (cachedData == null) {
            remoteDataSource.provideChuckNorrisData()
        } else {
            Single.just(cachedData)
        }
    }

    open fun markCacheAsDirty() {
        cachedData = null
    }
}

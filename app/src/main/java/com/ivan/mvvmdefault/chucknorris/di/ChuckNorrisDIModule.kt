package com.ivan.mvvmdefault.chucknorris.di

import com.ivan.coopcrossword.utils.schedulers.BaseSchedulerProvider
import com.ivan.mvvmdefault.api.ApiClient
import com.ivan.mvvmdefault.api.ApiInterface
import com.ivan.mvvmdefault.chucknorris.ChuckNorrisViewModel
import com.ivan.mvvmdefault.chucknorris.data.ChuckNorrisDataSource
import com.ivan.mvvmdefault.chucknorris.data.ChuckNorrisRepository
import com.ivan.mvvmdefault.chucknorris.data.remote.ChuckNorrisRemoteDataSource
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val profileViewModel = module {
    viewModel {
        ChuckNorrisViewModel(get(), get())
    }
}

val profileRepositoryModule = module {
    fun provideRepository(remoteDataSource: ChuckNorrisDataSource): ChuckNorrisRepository {
        return ChuckNorrisRepository(remoteDataSource)
    }
    single { provideRepository(get()) }
}

val profileDataSourceModule = module {
    fun provideChuckNorrisDataSourceModule(api: ApiInterface): ChuckNorrisDataSource {
        return ChuckNorrisRemoteDataSource(api)
    }
    single { provideChuckNorrisDataSourceModule(get()) }
}

val apiClient = module {
    single { ApiClient.getClient() }
}

val rxScheduleProvider = module {
    fun provideScheduleProvider(): BaseSchedulerProvider {
        return SchedulerProvider.instance
    }
    single { provideScheduleProvider() }
}
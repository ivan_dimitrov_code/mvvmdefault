package com.ivan.mvvmdefault.chucknorris.data

import com.ivan.mvvmdefault.chucknorris.model.internal.ChuckNorrisModel
import io.reactivex.Single

interface ChuckNorrisDataSource {
    fun provideChuckNorrisData(): Single<ChuckNorrisModel>
}

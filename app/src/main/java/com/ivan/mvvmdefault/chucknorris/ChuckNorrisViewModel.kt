package com.ivan.mvvmdefault.chucknorris

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.OnLifecycleEvent
import androidx.lifecycle.ViewModel
import com.ivan.coopcrossword.utils.schedulers.BaseSchedulerProvider
import com.ivan.mvvmdefault.chucknorris.data.ChuckNorrisRepository
import com.ivan.mvvmdefault.chucknorris.ui.ChuckNorrisState
import com.ivan.mvvmdefault.chucknorris.ui.DefaultState
import com.ivan.mvvmdefault.chucknorris.ui.ErrorState
import com.ivan.mvvmdefault.chucknorris.ui.LoadingState
import io.reactivex.disposables.CompositeDisposable

class ChuckNorrisViewModel(
    private val chucknorrisRepository: ChuckNorrisRepository,
    private val baseSchedulerProvider: BaseSchedulerProvider
) : ViewModel() {
    val uiState = MutableLiveData<ChuckNorrisState>()
    private val disposable = CompositeDisposable()

    fun requestChuckNorrisData() {
        uiState.value = LoadingState(false)
        val requestDisposable = chucknorrisRepository.provideChuckNorrisData()
            .observeOn(baseSchedulerProvider.ui())
            .subscribeOn(baseSchedulerProvider.io())
            .subscribe({
                uiState.value = DefaultState(true, it)
            }, {
                uiState.value = it.message?.let { errorMessage ->
                    ErrorState(errorMessage, false)
                }
            })

        disposable.add(requestDisposable)
    }

    fun requestNewJoke() {
        chucknorrisRepository.markCacheAsDirty()
        requestChuckNorrisData()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    fun clearDisposable() {
        disposable.clear()
    }
}

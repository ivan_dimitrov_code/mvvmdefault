package com.ivan.mvvmdefault.chucknorris.model

import com.ivan.mvvmdefault.chucknorris.model.external.ChuckNorrisServerModel
import com.ivan.mvvmdefault.chucknorris.model.internal.ChuckNorrisModel

fun adaptServerModelToAppModel(serverModel: ChuckNorrisServerModel): ChuckNorrisModel {
    return ChuckNorrisModel(serverModel.id, serverModel.url, serverModel.value)
}
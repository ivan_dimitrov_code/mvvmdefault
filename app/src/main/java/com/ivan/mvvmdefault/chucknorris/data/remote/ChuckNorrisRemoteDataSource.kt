package com.ivan.mvvmdefault.chucknorris.data.remote

import com.ivan.mvvmdefault.api.ApiInterface
import com.ivan.mvvmdefault.chucknorris.data.ChuckNorrisDataSource
import com.ivan.mvvmdefault.chucknorris.model.adaptServerModelToAppModel
import com.ivan.mvvmdefault.chucknorris.model.internal.ChuckNorrisModel
import io.reactivex.Single

class ChuckNorrisRemoteDataSource(val api: ApiInterface) : ChuckNorrisDataSource {
    override fun provideChuckNorrisData(): Single<ChuckNorrisModel> {
        return api.requestCrossword().map {
            adaptServerModelToAppModel(it)
        }
    }
}

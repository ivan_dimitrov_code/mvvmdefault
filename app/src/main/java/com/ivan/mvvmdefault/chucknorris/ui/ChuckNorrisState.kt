package com.ivan.mvvmdefault.chucknorris.ui

import com.ivan.mvvmdefault.chucknorris.model.internal.ChuckNorrisModel

sealed class ChuckNorrisState {
    abstract val loadedAllItems: Boolean
}

data class DefaultState(override val loadedAllItems: Boolean, val chuckNorris: ChuckNorrisModel) : ChuckNorrisState()
data class LoadingState(override val loadedAllItems: Boolean) : ChuckNorrisState()
data class ErrorState(val errorMessage: String, override val loadedAllItems: Boolean) : ChuckNorrisState()

package com.ivan.mvvmdefault

import android.app.Application
import com.ivan.mvvmdefault.chucknorris.di.*
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class AppApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        setupKoin()
    }

    private fun setupKoin() {
        startKoin {
            androidContext(this@AppApplication)
            modules(
                listOf(
                    rxScheduleProvider,
                    profileViewModel,
                    profileRepositoryModule,
                    profileDataSourceModule,
                    apiClient
                )
            )
        }
    }
}
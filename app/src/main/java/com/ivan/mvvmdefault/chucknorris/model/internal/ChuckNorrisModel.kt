package com.ivan.mvvmdefault.chucknorris.model.internal

data class ChuckNorrisModel(
    val id: String,
    val url: String,
    val value: String
)
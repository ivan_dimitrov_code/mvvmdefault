package com.ivan.mvvmdefault.api

import com.ivan.mvvmdefault.chucknorris.model.external.ChuckNorrisServerModel
import com.ivan.mvvmdefault.chucknorris.model.internal.ChuckNorrisModel
import io.reactivex.Single
import retrofit2.http.GET

interface ApiInterface {
    @GET("jokes/random")
    fun requestCrossword(): Single<ChuckNorrisServerModel>
}
package com.ivan.mvvmdefault.chucknorris

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.ivan.coopcrossword.utils.schedulers.BaseSchedulerProvider
import com.ivan.mvvmdefault.chucknorris.data.ChuckNorrisRepository

class ChuckNorrisViewModelFactory(
    private val chucknorrisRepository: ChuckNorrisRepository,
    private val baseSchedulerProvider: BaseSchedulerProvider
) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return ChuckNorrisViewModel(chucknorrisRepository, baseSchedulerProvider) as T
    }
}

package com.ivan.mvvmdefault.chucknorris.model.external

data class ChuckNorrisServerModel(
    val id: String,
    val url: String,
    val value: String
)